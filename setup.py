from setuptools import setup
setup(
  name = 'my_package',
  version = '1.3',
  description = 'This library allows users to do something',
  url = 'https://github.com/my_github_username/my_package',
  author = 'My Name Here',
  author_email = 'my_email@mail.com',
  packages = ['my_package'],
  include_package_data=True,

  keywords = ['my', 'package'],
  python_requires='~=3.6',
  install_requires=[
  'some_dependency==1.0'
  ],

)
